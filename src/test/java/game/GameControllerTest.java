package game;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.io.InputStream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldLocatePlayersInTheGroups_forGivenListOfClans() throws Exception {
        //given
        String content = readResourceAsString("clans_request.json");
        String expectedResponse = readResourceAsString("clans_response.json");

        //when
        this.mockMvc.perform(post("/onlinegame/calculate")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(expectedResponse));
    }

    static InputStream readResourceAsStream(String fileName) {
        return Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
    }

    static String readResourceAsString(String fileName) throws IOException {
        try (InputStream resourceAsStream = readResourceAsStream(fileName)) {
            byte[] response = IOUtils.toByteArray(resourceAsStream);
            return new String(response);
        }
    }
}
