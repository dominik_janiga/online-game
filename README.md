# Online game

## Spis treści
* [Opis zadania](#opis-zadania)
* [Uruchomienie aplikacji](#uruchomienie-aplikacji)
* [Przykład zapytania](#przykład-zapytania)
* [Przykład odpowiedzi](#przykład-odpowiedzi)
* [Technologie](#technologie)

## Opis zadania

Pewna świetna gra online w ostatnim czasie stała się bardzo popularna, a liczba graczy rośnie w szybkim tempie.
Oprócz standardowej rozgrywki, co jakiś czas odbywają się eventy specjalne, na których można zdobyć najwięcej punktów. Każdy gracz chce wziąć w nich udział.
Niestety, platforma na której uruchomiona jest gra, miewa problemy wydajnościowe dlatego wejście na plansze eventu odbywa się grupowo.
W grze można tworzyć klany, stworzone z większej ilości graczy. Siłę klanu definiuje suma punktów członków klanu.
Kolejność wejścia na event specjalny zależy od sumy punktów członków klanu.

Niestety, z powodu wyżej wspomnianych problemów wydajnościowych, wejście na planszę odbywa się zgodnie z poniższymi zasadami:
- gracze wpuszczani są grupami o maksymalnym rozmiarze (m)
- gracze z danej grupy wchodzą na planszę po kolei, a nie w jednym momencie, co oznacza że najlepsi gracze wejdą na serwer pierwsi
- członkowie klanu muszą koniecznie wejść razem w jednej grupie, aby móc korzystać ze swojej najsilniejszej broni, dlatego nie można ich dzielić
- grupy trzeba zoptymalizować w taki sposób, aby wpuszczać jak największą ilość graczy podczas jednego wejścia
- jeśli cała grupa się nie zmieści w danym wejściu, to pierwszeństwo ma kolejna grupa z mniejszą liczbą punktów
- jeśli dwa klany mają taką samą ilość punktów, to większy priorytet ma klan z mniejszą liczbą graczy (co oznacza, że klan ma silniejszych graczy)
- wszystkie klany muszą się dostać na event

Napisz algorytm, który dla zadanej liczby miejsc w grupie (m) oraz klanów wraz z jej liczebnością (l) i łączną liczbą punktów (p) wyznaczy kolejność oraz układ w jakich gracze powinny wejść na planszę.
Liczba klanów może być naprawdę długa, nawet do 20 000.

Liczby przyjmują wartości zgodnie z poniższymi przedziałami:
m=<1,1000>
l=<1,1000>
p=<1,100 000>

Warunki:
l <= m

## Uruchomienie aplikacji
```
git clone https://gitlab.com/dominik_janiga/online-game.git
cd online-game
bash build.sh
bash run.sh
```

## Przykład zapytania

```
{
  "groupCount": 6,
  "clans": [
    {
      "numberOfPlayers": 4,
      "points": 50
    },
    {
      "numberOfPlayers": 2,
      "points": 70
    },
    {
      "numberOfPlayers": 6,
      "points": 60
    },
    {
      "numberOfPlayers": 1,
      "points": 15
    },
    {
      "numberOfPlayers": 5,
      "points": 40
    },
    {
      "numberOfPlayers": 3,
      "points": 45
    },
    {
      "numberOfPlayers": 1,
      "points": 12
    },
    {
      "numberOfPlayers": 4,
      "points": 40
    }
  ]
}

```

## Przykład odpowiedzi

```
[
  [
    {
      "numberOfPlayers": 2,
      "points": 70
    },
    {
      "numberOfPlayers": 4,
      "points": 50
    }
  ],
  [
    {
      "numberOfPlayers": 6,
      "points": 60
    }
  ],
  [
    {
      "numberOfPlayers": 3,
      "points": 45
    },
    {
      "numberOfPlayers": 1,
      "points": 15
    },
    {
      "numberOfPlayers": 1,
      "points": 12
    }
  ],
  [
    {
      "numberOfPlayers": 4,
      "points": 40
    }
  ],
  [
    {
      "numberOfPlayers": 5,
      "points": 40
    }
  ]
]
```
## Technologie

Java 17

Spring Boot 3.0.5

Maven 3.9.0
